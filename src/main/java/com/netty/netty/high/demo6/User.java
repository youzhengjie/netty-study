package com.netty.netty.high.demo6;

import java.io.Serializable;

public class User implements Serializable {

    private String username;
    private int state; //用户状态，0在线，1下线

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", state=" + state +
                '}';
    }
}
