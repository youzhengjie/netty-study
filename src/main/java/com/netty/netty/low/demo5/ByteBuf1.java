package com.netty.netty.low.demo5;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

import java.io.UnsupportedEncodingException;

import static io.netty.buffer.ByteBufUtil.appendPrettyHexDump;
import static io.netty.util.internal.StringUtil.NEWLINE;

public class ByteBuf1 {

    private static void log(ByteBuf buffer) {
        int length = buffer.readableBytes();
        int rows = length / 16 + (length % 15 == 0 ? 0 : 1) + 4;
        StringBuilder buf = new StringBuilder(rows * 80 * 2)
                .append("read index:").append(buffer.readerIndex())
                .append(" write index:").append(buffer.writerIndex())
                .append(" capacity:").append(buffer.capacity())
                .append(NEWLINE);
        appendPrettyHexDump(buf, buffer);
        System.out.println(buf.toString());
    }

  public static void main(String[] args) throws UnsupportedEncodingException {

//      //创建ByteBuf
//      ByteBuf byteBuf = ByteBufAllocator.DEFAULT.buffer(10);
//
//      log(byteBuf);
//
//      StringBuffer stringBuffer = new StringBuffer();
//
//    for (int i = 0; i < 50; i++) {
//      stringBuffer.append('1');
//    }
//    byteBuf.writeBytes(stringBuffer.toString().getBytes("utf-8"));
//    log(byteBuf);


      ByteBuf byteBuf1 = ByteBufAllocator.DEFAULT.buffer(10); //默认创建的是‘’直接内存‘’的ByteBuf

      log(byteBuf1);
      byteBuf1.writeBytes("helloaaaaaaaa".getBytes("utf-8"));

      log(byteBuf1);





  }
}
