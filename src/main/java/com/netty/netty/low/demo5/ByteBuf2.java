package com.netty.netty.low.demo5;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

import java.io.UnsupportedEncodingException;

public class ByteBuf2 {

  public static void main(String[] args) throws UnsupportedEncodingException {

      ByteBuf byteBuf = ByteBufAllocator.DEFAULT.buffer(10);

      byteBuf.writeBytes("hello".getBytes("utf-8"));

      byteBuf.release();
  }
}
