package com.netty.netty.low.demo5;

import com.netty.util.ByteBufferUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

import java.io.UnsupportedEncodingException;

public class ByteBuf3 {

  public static void main(String[] args) throws UnsupportedEncodingException {

      ByteBuf byteBuf = ByteBufAllocator.DEFAULT.buffer(10);

      byteBuf.writeBytes("helloWorld".getBytes("utf-8"));

      ByteBufferUtil.log(byteBuf);

      ByteBuf buf = byteBuf.slice(0, 5); //内存分片

      ByteBufferUtil.log(buf);

      System.out.println("---------------");

      buf.setByte(1,'g'); //修改分片内存的值

      //重新打印
      ByteBufferUtil.log(byteBuf);

      ByteBufferUtil.log(buf);

  }
}
