package com.netty.netty.low.demo2;

import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;

import java.util.concurrent.TimeUnit;

public class EvenLoopMain {

  public static void main(String[] args) {
      EventLoopGroup ev=new NioEventLoopGroup(3);

//      System.out.println(ev.next().hashCode());
//      System.out.println(ev.next().hashCode());
//      System.out.println(ev.next().hashCode());
//      System.out.println(ev.next().hashCode());

      //普通任务
      ev.next().submit(()->{

          System.out.println("111");

      });

      System.out.println("222");

      //定时任务
      ev.next().scheduleAtFixedRate(()->{

          System.out.println("333");

      },0,1,TimeUnit.SECONDS);




  }
}
