package com.netty.netty.low.demo2;

import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;

public class CloseEvenLoopGroup {

  public static void main(String[] args) {

      EventLoopGroup eventLoopGroup = new NioEventLoopGroup();


      eventLoopGroup.shutdownGracefully(); //优雅的关闭EventLoopGroup


  }
}
