package com.netty.netty.low.demo3;

import io.netty.channel.EventLoop;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.util.concurrent.DefaultPromise;

import java.util.concurrent.ExecutionException;

public class NettyPromise {

  public static void main(String[] args) throws ExecutionException, InterruptedException {

      NioEventLoopGroup eventLoopGroup = new NioEventLoopGroup();
      EventLoop executors = eventLoopGroup.next();

      DefaultPromise<Integer> promise = new DefaultPromise<>(executors);


      new Thread(()->{

          try {
              Thread.sleep(1000);
          } catch (InterruptedException e) {
              e.printStackTrace();
          }
          promise.setSuccess(100);

      }).start();


      Integer res = promise.get();
      System.out.println(res);


  }
}
