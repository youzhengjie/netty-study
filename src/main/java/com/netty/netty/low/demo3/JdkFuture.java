package com.netty.netty.low.demo3;

import java.util.concurrent.*;

public class JdkFuture {

  public static void main(String[] args) throws ExecutionException, InterruptedException {

      ExecutorService executorService = Executors.newFixedThreadPool(2); //创建一个固定大小的线程池

      //Callable有返回值。
      Future<String> future = executorService.submit(new Callable<String>() {
          @Override
          public String call() throws Exception {
              Thread.sleep(1000);
              return "hello";
          }
      });

      String res = future.get(); //get方法会阻塞，直到线程池的submit执行完毕，返回了future对象才会解除阻塞
      System.out.println(res);

      executorService.shutdown(); //关闭线程池

  }
}
