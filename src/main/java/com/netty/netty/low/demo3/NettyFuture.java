package com.netty.netty.low.demo3;

import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.util.concurrent.Future;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

public class NettyFuture {

  public static void main(String[] args) throws ExecutionException, InterruptedException {

      EventLoopGroup eventLoopGroup = new NioEventLoopGroup(2);

      Future<String> future = eventLoopGroup.next().submit(new Callable<String>() {
          @Override
          public String call() throws Exception {
              Thread.sleep(1000);
              return "Netty Future";
          }
      });

//      String s1 = future.get(); //阻塞方法，这个方法和jdk的future一样
//      System.out.println(s1);

      String s2 = future.getNow(); //非阻塞方法，如果future没有立刻返回值则不会等待，直接返回null
      System.out.println(s2);


  }
}
