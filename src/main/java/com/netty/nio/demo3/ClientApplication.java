package com.netty.nio.demo3;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

public class ClientApplication {

  public static void main(String[] args) throws IOException {


    SocketChannel socketChannel = SocketChannel.open(new InetSocketAddress(8081));

    ByteBuffer byteBuffer = StandardCharsets.UTF_8.encode("hello");
    socketChannel.write(byteBuffer);

  }
}
