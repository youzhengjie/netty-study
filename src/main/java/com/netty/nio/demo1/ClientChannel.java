package com.netty.nio.demo1;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

public class ClientChannel {

  public static void main(String[] args) throws IOException {

      SocketChannel socketChannel = SocketChannel.open(new InetSocketAddress( 8080));

      ByteBuffer byteBuffer = StandardCharsets.UTF_8.encode("this is nio");
      socketChannel.write(byteBuffer);



  }


}
