package com.netty.nio.demo1;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

public class ServerChannel {

    //nio实现网络通信

  public static void main(String[] args) throws IOException {


      ServerSocketChannel serverSocketChannel = ServerSocketChannel.open(); //打开serverSocketChannel

      serverSocketChannel.bind(new InetSocketAddress(8080));

      while (true){

      System.out.println("waiting.....");
          SocketChannel socketChannel = serverSocketChannel.accept(); //阻塞
      System.out.println("connect success");
          ByteBuffer byteBuffer = ByteBuffer.allocate(100);
          socketChannel.read(byteBuffer); //阻塞，等待消息发送过来即可封装到缓存里去
          byteBuffer.flip();
      System.out.println(StandardCharsets.UTF_8.decode(byteBuffer).toString());


      }




  }
}
