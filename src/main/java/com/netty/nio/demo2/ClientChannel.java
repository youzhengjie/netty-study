package com.netty.nio.demo2;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

public class ClientChannel {

  public static void main(String[] args) throws IOException {

      SocketChannel socketChannel = SocketChannel.open(new InetSocketAddress(8082));

      ByteBuffer byteBuffer = StandardCharsets.UTF_8.encode("hello");
      socketChannel.write(byteBuffer);

  }
}
